public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;

    public Triangle(double a,double b,double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public String toString() {
        return this.getName() + " Side A : " + this.a + " Side B : " + this.b + " Side C : " + this.c ;
    }

    @Override
    public double calArea() {
        double S = (a + b + c)/2;
        double tanS = (S-a)*(S-b)*(S-c);
        double areas = Math.sqrt(S*tanS);
        return areas;
    }

    @Override
    public double calPerimeter() {
        double perimeter = a+b+c;
        return perimeter;
        
    }
}
