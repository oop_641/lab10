public class Rectangle extends Shape {
    private double widht;
    private double height;

    public Rectangle(double widht, double height) {
        super("Rectangle");
        this.widht = widht;
        this.height = height;
    }

    public double getWidth() {
        return widht;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return this.getName() + " width : " + this.widht + " height : " + this.height;
    }

    @Override
    public double calArea() {
        return this.widht * this.height;
    }

    @Override
    public double calPerimeter() {
        return  this.widht*2 + this.height*2;
    }
}
