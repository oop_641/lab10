import org.w3c.dom.css.Rect;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.printf("%s %.3f \n" , rec.getName(), rec.calArea());
        System.out.printf("%s %.3f \n" , rec.getName(),rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.printf("%s %.3f \n" , rec.getName(),rec2.calArea());
        System.out.printf("%s %.3f \n" , rec.getName(),rec2.calPerimeter());

        Circle cir = new Circle(2);
        System.out.println(cir.toString());
        System.out.printf("%s %.3f \n", cir.getName(), cir.calArea());
        System.out.printf("%s %.3f \n", cir.getName(),cir.calPerimeter());

        Circle cir2 = new Circle(3);
        System.out.println(cir2.toString());
        System.out.printf("%s %.3f \n", cir.getName(),cir2.calArea());
        System.out.printf("%s %.3f \n", cir.getName(),cir2.calPerimeter());

        Triangle tri = new Triangle(2, 2, 2);
        System.out.println(tri.toString());
        System.out.printf("%s %.3f \n", tri.getName(),tri.calArea());
        System.out.printf("%s %.3f \n", tri.getName(),tri.calPerimeter());
    }
}
